﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed = 10f;
    public float turningSpeed = 80f;
    
    private float _moveHorizontal;
    private float _moveVertical;
    private CharacterController _playerController;
    private int _currentHealth;
	// Use this for initialization
	void Start ()
    {
        _playerController = GetComponent<CharacterController>();
        _playerController.detectCollisions = true;
        _currentHealth = GameManager.Instance.maxPlayerHealth;
    }
	
	// Update is called once per frame
	void Update ()
    {
        _moveHorizontal = Input.GetAxis("Horizontal");
        _moveVertical = Input.GetAxis("Vertical");

        _playerController.Move(_moveVertical * Time.deltaTime * speed * transform.TransformDirection(Vector3.forward));
        transform.Rotate(Vector3.up, _moveHorizontal * Time.deltaTime * turningSpeed);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Projectile"))
        {
            other.gameObject.SetActive(false);
            _currentHealth -= GameManager.Instance.playerHealthLostPerAttack;

            GameManager.Instance.UpdatePlayerHealth(_currentHealth);

            if (_currentHealth == 0)
            {
                GameManager.Instance.GameOver();
            }
            
        }
    }
}
