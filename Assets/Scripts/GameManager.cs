﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public static GameManager Instance = null;

    public PlayerController playerController;
    public GameObject[] bullets;
    public GameObject messagePanel;
    public GameObject startButton;
    public GameObject reloadButton;
    public Text messageText;
    public Text playerHealthText;
    public Text gameTimeText;


    public float projectileSpeed = 5;
    public float delayBetweenEnemyFire = 2f;
    public int playerHealthLostPerAttack = 10;
    public int maxPlayerHealth = 100;
    public int gameTimeInSeconds = 60;

    private bool _gameOver;
    private int _currentGameTime;
   
    private void Awake()
    {
        //Singleton Implementation of GameManager
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);
    }

    // Use this for initialization
    void Start () {
        // Pause the game until user starts it 
        Time.timeScale = 0.0000001f;
        _gameOver = false;
        playerHealthText.text = maxPlayerHealth.ToString();
        startButton.SetActive(true);
        messagePanel.SetActive(true);
    }
	
	public void UpdatePlayerHealth(int currentHealth)
    {
        playerHealthText.text = currentHealth.ToString();
    }

    public void GameStart()
    {
        Time.timeScale = 1;
        _currentGameTime = gameTimeInSeconds;
        RunGameTimeUpdate();
        messagePanel.SetActive(false);
    }

    void RunGameTimeUpdate()
    {
        if (!_gameOver)
        {
            if (_currentGameTime == 0)
            {
                GameWon();
            }
            else
            {
                StartCoroutine(UpdateGameTime());
            }

        }
    }

    IEnumerator UpdateGameTime()
    {
        yield return new WaitForSeconds(1);
        _currentGameTime--;
        gameTimeText.text = _currentGameTime.ToString();
        RunGameTimeUpdate();
    }

    public void Reload()
    {
        SceneManager.LoadScene("Main");
    }

    public void GameOver()
    {
        _gameOver = true;
        Time.timeScale = 0.0000001f;
        messageText.text = "Game Over";
        
        startButton.SetActive(false);
        reloadButton.SetActive(true);
        messagePanel.SetActive(true);
    }

    public void GameWon()
    {
        messageText.text = "You Won";
        
        startButton.SetActive(false);
        reloadButton.SetActive(true);
        messagePanel.SetActive(true);
    }
}
