﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    public float viewDistance = 15; // view radius of the enemy
    public float attackDistance = 8; // attack radius of the enemy


    private GameObject[] bullets; 
    private EnemyStates currentState;
    private GameManager gameManager;
    private Renderer headMatRenderer; // for alert state we change material color to indicate enemy's alert state

    private Transform _player;
    private Color _idleColor;
    private int _highObstacleCounter;
    private int _lowObstacleCounter;
    private bool _isChasing;
    private float _previousFireTime;


    private Vector3 stationPoint; // point where enemy was originally stationed in game
   

    public enum EnemyStates { Idle, Alert, Chasing, Attack, Reset } // states of enemy inside the game

    // Use this for initialization
	void Start ()
    {
        currentState = EnemyStates.Idle;
        headMatRenderer = transform.Find("Head").GetComponent<Renderer>();
        _idleColor = headMatRenderer.material.color;
        _player = GameObject.FindGameObjectWithTag("Player").transform;
        _highObstacleCounter = 0;
        _lowObstacleCounter = 0;
        _previousFireTime = Time.time;
        stationPoint = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        bullets = GameManager.Instance.bullets;
    }
	
	// Update is called once per frame
	void Update () {
        // set idle state to enemy by default
        currentState = EnemyStates.Idle;
        headMatRenderer.material.color = _idleColor;

        // detect and be aware of the distance of player and set status accordingly
        DetectPlayerDistance();
        // check the transition and handle the enemy state transitions
        CheckAndUpdateStatus();

        // if enemy went back to idle after an chase attack sequence, move him to his station point 
        if (_isChasing && currentState == EnemyStates.Idle)
        {
            if (transform.position == stationPoint)
            {
                _isChasing = false;
            }
            else
            {
                transform.position = Vector3.MoveTowards(transform.position, stationPoint, 0.5f * Time.deltaTime);
            }
        }
        else if (currentState == EnemyStates.Chasing)
        {
            // chasing the player when he is view radius and no obstructions are there
            transform.position = Vector3.MoveTowards(transform.position, _player.position ,0.5f * Time.deltaTime); 
        }
    }

    

    void CheckAndUpdateStatus()
    {
        _highObstacleCounter = _lowObstacleCounter = 0;

        if (currentState == EnemyStates.Alert || currentState == EnemyStates.Attack)
        {
            CheckForObstacles();
            
            if (_highObstacleCounter > 0 && currentState == EnemyStates.Alert)
            {
                currentState = EnemyStates.Idle;
            }
            else if (_highObstacleCounter > 0 && currentState == EnemyStates.Attack)
            {
                currentState = EnemyStates.Idle;
            }

            if (currentState == EnemyStates.Alert && _lowObstacleCounter == 0)
            {
                currentState = EnemyStates.Chasing;
                _isChasing = true;
                Debug.Log("Chasing");
            }
            else if (currentState == EnemyStates.Alert)
            {
                headMatRenderer.material.color = Color.black;
            }
            else if (currentState == EnemyStates.Attack)
            {
                Debug.Log("Attacking");
                Attack();
            }
        }
    }



    void Attack()
    {
        // Rotate the enemy towards player before shooting bullets
        Vector3 targetDir = _player.position - transform.position;
        transform.rotation = Quaternion.LookRotation(targetDir);
       
        // check for available bullets in bullets pool (Object Pooling) 
        GameObject[] availableBullets = System.Array.FindAll(bullets, x => x.gameObject.activeSelf == false);
        if ((Time.time - _previousFireTime > GameManager.Instance.delayBetweenEnemyFire) && availableBullets.Length > 0)
        {
            _previousFireTime = Time.time;

            availableBullets[0].transform.position = transform.position;
            availableBullets[0].transform.rotation = Quaternion.LookRotation(targetDir);
            availableBullets[0].SetActive(true);
            availableBullets[0].GetComponent<Rigidbody>().AddForce(transform.forward * GameManager.Instance.projectileSpeed, ForceMode.VelocityChange);
        }
    }

    void CheckForObstacles()
    {
        float distanceToPlayer = Vector3.Distance(transform.position, _player.position);

        RaycastHit[] highObstaclehits;
        RaycastHit[] lowObstaclehits;

        Vector3 enemyLowPosition = new Vector3(transform.position.x, transform.position.y - 0.33f, transform.position.z);
        // look for all high obstacles between enemy and player
        highObstaclehits = Physics.RaycastAll(transform.position, _player.position, distanceToPlayer);
        // look for all low obstacles between enemy and player
        lowObstaclehits = Physics.RaycastAll(enemyLowPosition, _player.position, distanceToPlayer);

        
        for (int i = 0; i < highObstaclehits.Length; i++)
        {
            RaycastHit hit = highObstaclehits[i];
            if (hit.transform.gameObject.tag.Equals("HighObstacle"))
            {
               _highObstacleCounter++;
            }
        }

        for (int i = 0; i < lowObstaclehits.Length; i++)
        {
            RaycastHit hit = lowObstaclehits[i];
            if (hit.transform.gameObject.tag.Equals("LowObstacle"))
            {
                _lowObstacleCounter++;
            }
        }
    }

    void DetectPlayerDistance()
    {
        Vector3 playerPosition = _player.position;

        float factoredViewDistance = viewDistance / 10; // distance factored by 10 to achieve game scale distance 
        float factoredAttackDistance = attackDistance / 10; // distance factored by 10 to achieve game scale distance 

        // Algorithm to find whether player lies within the radius of view or attack perimeter of enemy. (Equation of circle : (x - h)^2 + (y - k)^2 < radius ^ 2)
        float radiusToPlayer = Mathf.Pow(playerPosition.x - transform.position.x, 2) + Mathf.Pow(playerPosition.z - transform.position.z, 2);

        //check whether player is within view perimeter
        if (currentState == EnemyStates.Idle && (radiusToPlayer < Mathf.Pow(factoredViewDistance, 2)))
        {
            currentState = EnemyStates.Alert;
        }
        //check whether player is within attack perimeter
        if ((currentState == EnemyStates.Chasing || currentState == EnemyStates.Alert) && (radiusToPlayer < Mathf.Pow(factoredAttackDistance, 2)))
        {
            currentState = EnemyStates.Attack;
        }
    }

}
